package shivamkumarjha.imagedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button b;
    ImageView i;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = (Button) findViewById(R.id.chn);
        i = (ImageView) findViewById(R.id.img);

        b.setText("START");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (count==1)
                {
                    i.setImageResource(R.drawable.a);
                    b.setText("CHANGE");
                }
                else if (count==2)
                    i.setImageResource(R.drawable.b);
                else if (count==3)
                    i.setImageResource(R.drawable.c);
                else if (count==4)
                    i.setImageResource(R.drawable.d);
                else if (count==5)
                    i.setImageResource(R.drawable.e);
                else if (count==6)
                    i.setImageResource(R.drawable.f);
                else if (count==7)
                    i.setImageResource(R.drawable.g);
                else {
                    Toast.makeText(MainActivity.this, "THATS IT!",Toast.LENGTH_SHORT).show();
                    b.setText("QUIT");
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MainActivity.this.finish();
                            System.exit(0);
                        }
                    });
                }

            }
        });
    }
}
